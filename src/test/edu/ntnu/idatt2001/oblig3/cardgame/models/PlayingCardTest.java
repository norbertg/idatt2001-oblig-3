package edu.ntnu.idatt2001.oblig3.cardgame.models;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {
    @Test
    void constructor_faceLowerThan1_throwsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> new PlayingCard(Suit.HEARTS, 0)
        );
    }

    @Test
    void constructor_faceGreaterThan13_throwsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> new PlayingCard(Suit.HEARTS, 14)
        );
    }

    @Test
    void asString_returnsSuitAndFace() {
        PlayingCard playingCard = new PlayingCard(Suit.CLUBS, 6);

        String string = playingCard.asString();

        assertEquals("C6", string);
    }

    @Test
    void getSuit_returnsSuit() {
        PlayingCard playingCard = new PlayingCard(Suit.SPADES, 1);

        Suit suit = playingCard.getSuit();

        assertEquals(Suit.SPADES, suit);
    }

    @Test
    void getFace_returnsFace() {
        PlayingCard playingCard = new PlayingCard(Suit.SPADES, 10);

        int face = playingCard.getFace();

        assertEquals(10, face);
    }
}