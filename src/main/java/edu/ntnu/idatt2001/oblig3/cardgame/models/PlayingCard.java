package edu.ntnu.idatt2001.oblig3.cardgame.models;

/**
 * Represents a playing card. A card consists of a suit and a face.
 * The face represents the number on the card, while the suit represents the card
 * category (the symbol).
 *
 * @author Norbert Gørke
 */
public class PlayingCard {
    private final Suit suit;
    private final int face;

    /**
     * Constructor for PlayingCard.
     *
     * @param suit The suit of the card.
     * @param face The face value of the card in the range [1, 13]
     */
    public PlayingCard(Suit suit, int face) {
        if (face < 1 || face > 13) {
            throw new IllegalArgumentException("face must be in range [1, 13]");
        }
        this.suit = suit;
        this.face = face;
    }

    /**
     * Returns the playing card as a string of the format [SUIT][FACE].
     * Example: Heart 4 will be H4.
     *
     * @return the suit and face of the card as a string
     */
    public String asString() {
        return String.format("%s%s", suit, face);
    }

    public Suit getSuit() {
        return suit;
    }

    public int getFace() {
        return face;
    }
}