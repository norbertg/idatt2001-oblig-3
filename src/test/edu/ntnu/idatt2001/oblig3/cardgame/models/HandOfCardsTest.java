package edu.ntnu.idatt2001.oblig3.cardgame.models;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {
    @Test
    void getCards_returnsCards() {
        List<PlayingCard> expected = Arrays.asList(
            new PlayingCard(Suit.HEARTS, 10),
            new PlayingCard(Suit.SPADES, 5)
        );
        HandOfCards hand = new HandOfCards(expected);

        List<PlayingCard> actual = hand.getCards();

        assertEquals(expected, actual);
    }

    @Test
    void sumOfCards_returnsSumOfCardFaces() {
        HandOfCards hand = new HandOfCards(List.of(
            new PlayingCard(Suit.HEARTS, 10),
            new PlayingCard(Suit.DIAMONDS, 5),
            new PlayingCard(Suit.CLUBS, 3)
        ));

        int sum = hand.sumOfCardFaces();

        assertEquals(18, sum);
    }

    @Test
    void cardsWithSuit_returnsListOfCardsWithSpecificSuit() {
        PlayingCard firstCard = new PlayingCard(Suit.HEARTS, 10);
        PlayingCard secondCard = new PlayingCard(Suit.HEARTS, 3);
        List<PlayingCard> expected = List.of(firstCard, secondCard);
        HandOfCards hand = new HandOfCards(List.of(
            firstCard,
            new PlayingCard(Suit.DIAMONDS, 5),
            new PlayingCard(Suit.CLUBS, 3),
            secondCard
        ));

        List<PlayingCard> cards = hand.cardsWithSuit(Suit.HEARTS);

        assertEquals(expected, cards);
    }

    @Test
    void hasFiveFlush_hasFiveCardsWithSameSuit_returnsTrue() {
        HandOfCards hand = new HandOfCards(List.of(
            new PlayingCard(Suit.HEARTS, 1),
            new PlayingCard(Suit.HEARTS, 7),
            new PlayingCard(Suit.HEARTS, 3),
            new PlayingCard(Suit.HEARTS, 9),
            new PlayingCard(Suit.HEARTS, 13)
        ));

        boolean hasFlush = hand.hasFiveFlush();

        assertTrue(hasFlush);
    }

    @Test
    void hasFiveFlush_doesNotHaveFiveCardsWithSameSuit_returnsFalse() {
        HandOfCards hand = new HandOfCards(List.of(
            new PlayingCard(Suit.HEARTS, 1),
            new PlayingCard(Suit.HEARTS, 7),
            new PlayingCard(Suit.HEARTS, 3),
            new PlayingCard(Suit.HEARTS, 9),
            new PlayingCard(Suit.CLUBS, 13)
        ));

        boolean hasFlush = hand.hasFiveFlush();

        assertFalse(hasFlush);
    }

    @Test
    void hasQueenOfSpades_hasQueenOfSpades_returnsTrue() {
        HandOfCards hand = new HandOfCards(List.of(
            new PlayingCard(Suit.HEARTS, 1),
            new PlayingCard(Suit.HEARTS, 7),
            new PlayingCard(Suit.SPADES, 12),
            new PlayingCard(Suit.HEARTS, 9),
            new PlayingCard(Suit.CLUBS, 13)
        ));

        boolean hasQueenOfSpades = hand.hasQueenOfSpades();

        assertTrue(hasQueenOfSpades);
    }

    @Test
    void hasQueenOfSpades_doesNotHaveQueenOfSpades_returnsFalse() {
        HandOfCards hand = new HandOfCards(List.of(
            new PlayingCard(Suit.HEARTS, 1),
            new PlayingCard(Suit.HEARTS, 7),
            new PlayingCard(Suit.SPADES, 11),
            new PlayingCard(Suit.HEARTS, 9),
            new PlayingCard(Suit.CLUBS, 13)
        ));

        boolean hasQueenOfSpades = hand.hasQueenOfSpades();

        assertFalse(hasQueenOfSpades);
    }
}