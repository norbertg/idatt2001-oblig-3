package edu.ntnu.idatt2001.oblig3.cardgame.controllers.controls;

import edu.ntnu.idatt2001.oblig3.cardgame.AppMain;
import edu.ntnu.idatt2001.oblig3.cardgame.models.PlayingCard;
import edu.ntnu.idatt2001.oblig3.cardgame.models.Suit;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.util.Map;

public class Card extends VBox {
    private static final String CARD_RESOURCE_NAME = "views/controls/Card.fxml";
    private static final Map<Suit, Color> suitColors = Map.of(
        Suit.SPADES, Color.BLACK,
        Suit.HEARTS, Color.RED,
        Suit.CLUBS, Color.BLACK,
        Suit.DIAMONDS, Color.RED
    );
    private static final Map<Suit, String> suitSymbols = Map.of(
        Suit.SPADES, "♠",
        Suit.HEARTS, "♥",
        Suit.DIAMONDS, "♦",
        Suit.CLUBS, "♣"
    );
    private static final Map<Integer, String> specialFaceNames = Map.of(
        1, "A",
        11, "J",
        12, "Q",
        13, "K"
    );

    private PlayingCard playingCard;

    @FXML
    private Label suitLabel;

    @FXML
    private Label faceLabel;

    public Card() {
        FXMLLoader fxmlLoader = new FXMLLoader(AppMain.class.getResource(CARD_RESOURCE_NAME));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public Card(PlayingCard card) {
        this();
        setPlayingCard(card);
    }

    public void setPlayingCard(PlayingCard playingCard) {
        this.playingCard = playingCard;

        Suit suit = playingCard.getSuit();
        int face = playingCard.getFace();
        Color color = suitColors.getOrDefault(suit, Color.BLACK);
        String suitSymbol = suitSymbols.getOrDefault(suit, "SUIT");
        String faceSymbol = specialFaceNames.getOrDefault(face, Integer.toString(face));

        suitLabel.setText(suitSymbol);
        suitLabel.setTextFill(color);
        faceLabel.setText(faceSymbol);
        faceLabel.setTextFill(color);
    }

    public PlayingCard getPlayingCard() {
        return playingCard;
    }
}
