package edu.ntnu.idatt2001.oblig3.cardgame.models;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
    @Test
    void dealHand_5cards_returns5cards() {
        DeckOfCards deck = new DeckOfCards();

        List<PlayingCard> cards = deck.dealHand(5).getCards();

        assertEquals(5, cards.size());
    }

}