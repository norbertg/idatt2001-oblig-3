package edu.ntnu.idatt2001.oblig3.cardgame.models;

public enum Suit {
    SPADES('S'),
    HEARTS('H'),
    DIAMONDS('D'),
    CLUBS('C');

    private final char suit;
    Suit(char suit) {
        this.suit = suit;
    }

    @Override
    public String toString() {
        return Character.toString(suit);
    }
}
