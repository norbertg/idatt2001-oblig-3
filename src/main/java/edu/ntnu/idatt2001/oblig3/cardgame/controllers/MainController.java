package edu.ntnu.idatt2001.oblig3.cardgame.controllers;

import edu.ntnu.idatt2001.oblig3.cardgame.controllers.controls.Card;
import edu.ntnu.idatt2001.oblig3.cardgame.models.DeckOfCards;
import edu.ntnu.idatt2001.oblig3.cardgame.models.HandOfCards;
import edu.ntnu.idatt2001.oblig3.cardgame.models.PlayingCard;
import edu.ntnu.idatt2001.oblig3.cardgame.models.Suit;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;

import java.util.List;
import java.util.stream.Collectors;

public class MainController {
    @FXML
    private TextField faceSumTextField;
    @FXML
    private TextField cardsOfHeartsTextField;
    @FXML
    private TextField hasFlushTextField;
    @FXML
    private TextField hasQueenOfSpadesTextField;
    @FXML
    private TilePane cardContainer;

    private HandOfCards handOfCards = new HandOfCards();

    @FXML
    private void initialize() {
        Card card = new Card();
        card.setPlayingCard(new PlayingCard(Suit.HEARTS, 4));
    }

    public void checkHand() {
        String faceSumText = Integer.toString(handOfCards.sumOfCardFaces());
        String cardsOfHeartsText = handOfCards.cardsWithSuit(Suit.HEARTS).stream()
            .map(x -> x.asString())
            .reduce("", (acc, x) -> acc + " " + x)
            .trim();
        String hasFlushText = handOfCards.hasFiveFlush() ? "Yes" : "No";
        String hasQueenOfSpadesText = handOfCards.hasQueenOfSpades() ? "Yes" : "No";
        faceSumTextField.setText(faceSumText);
        cardsOfHeartsTextField.setText(cardsOfHeartsText);
        hasFlushTextField.setText(hasFlushText);
        hasQueenOfSpadesTextField.setText(hasQueenOfSpadesText);
    }

    public void dealHand(MouseEvent mouseEvent) {
        DeckOfCards deck = new DeckOfCards();
        handOfCards = deck.dealHand(10);
        cardContainer.getChildren().clear();
        cardContainer.getChildren().addAll(
            handOfCards.getCards().stream()
                .map(x -> new Card(x))
                .collect(Collectors.toList())
        );
    }
}
