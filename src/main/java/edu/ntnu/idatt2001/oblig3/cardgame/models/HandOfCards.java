package edu.ntnu.idatt2001.oblig3.cardgame.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {
    private final List<PlayingCard> cards;

    public HandOfCards(Collection<PlayingCard> cards) {
        this.cards = new ArrayList<>(cards);
    }

    public HandOfCards() {
        this.cards = List.of();
    }

    public List<PlayingCard> getCards() {
        return new ArrayList<>(cards);
    }

    public int sumOfCardFaces() {
        return cards.stream()
            .mapToInt(PlayingCard::getFace)
            .sum();
    }

    public List<PlayingCard> cardsWithSuit(Suit suit) {
        return cards.stream()
            .filter(x -> x.getSuit() == suit)
            .collect(Collectors.toList());
    }

    public boolean hasFiveFlush() {
        return Arrays.stream(Suit.values())
            .map(this::cardsWithSuit)
            .anyMatch(x -> x.size() >= 5);
    }

    public boolean hasQueenOfSpades() {
        return cards.stream()
            .filter(x -> x.getSuit() == Suit.SPADES)
            .anyMatch(x -> x.getFace() == 12);
    }
}
