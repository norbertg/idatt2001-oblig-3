package edu.ntnu.idatt2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppMain extends Application {
    private final static String TITLE = "Card game";
    private final static String MAIN_VIEW_RESOURCE_NAME = "views/MainView.fxml";
    private final static String STYLE_RESOURCE_NAME = "style.css";

    @Override
    public void start(Stage stage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource(MAIN_VIEW_RESOURCE_NAME));
        Scene scene = new Scene(parent);

        scene.getStylesheets().add(getClass().getResource(STYLE_RESOURCE_NAME).toExternalForm());

        stage.setTitle(TITLE);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
}
