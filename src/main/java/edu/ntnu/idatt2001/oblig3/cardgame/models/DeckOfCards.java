package edu.ntnu.idatt2001.oblig3.cardgame.models;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DeckOfCards {
    private final List<PlayingCard> cards;
    private final Random random = new Random();

    public DeckOfCards() {
        cards = Arrays.stream(Suit.values())
            .flatMap(suit -> IntStream.range(1, 14)
                .mapToObj(face -> new PlayingCard(suit, face)))
            .collect(Collectors.toList());
    }

    private PlayingCard pickRandomCard() {
        int index = random.nextInt(cards.size());
        PlayingCard card = cards.get(index);
        cards.remove(index);
        return card;
    }

    public HandOfCards dealHand(int cardAmount) {
        if (cardAmount < 1 || cardAmount > 52) {
            throw new IllegalArgumentException("cardAmount must be within [1, 52]");
        }
        if (cardAmount > cards.size()) {
            throw new IllegalStateException("cardAmount cannot be greater than amount of cards in deck");
        }
        List<PlayingCard> cards = IntStream.range(0, cardAmount)
            .mapToObj((x) -> pickRandomCard())
            .collect(Collectors.toList());
        return new HandOfCards(cards);
    }
}
